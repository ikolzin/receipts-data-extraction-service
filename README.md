Receipts Data Extraction - Service
==============================

Extracts good and brand data from receipts.

------------

This is a companion project to [Receipts Data Extraction](https://gitlab.com/ikolzin/receipts-data-extraction). It provides a way to serve trained models using FastAPI. Requires MLFlow server to be set up for model storage. 

-------------

**Installation:**

1. Clone the repository to a local folder
2. Rename .env.template to .env and fill in parameters according to your MLFlow and S3 setup.
3. Run `docker compose up -d --build`

The FastAPI service is now available at the port configured in the .env file. By default it comes with Swagger UI at /docs/ which you can use to test the model. 