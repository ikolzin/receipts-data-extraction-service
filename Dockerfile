FROM python:3.10

WORKDIR /code
ENV PYTHONPATH="$PYTHONPATH:/code/src"
RUN pip install --upgrade pip
RUN pip install poetry

COPY ./src/ /code/src/
COPY ./pyproject.toml /code/pyproject.toml
COPY ./poetry.lock /code/poetry.lock
RUN poetry install

COPY ./.env /code/.env

CMD ["poetry", "run", "uvicorn", "src.inference:app", "--host", "0.0.0.0", "--port", "80"]
