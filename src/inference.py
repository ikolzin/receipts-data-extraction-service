import io
from dotenv import load_dotenv
from fastapi import FastAPI, UploadFile, HTTPException
from fastapi.responses import StreamingResponse
from src.models.receipts_classification_model import Receipts_Classification_Model
from src.data.prepare_data import prepare_data
from prometheus_fastapi_instrumentator import Instrumentator
import logging
import sys
import os

load_dotenv()
log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
logging.basicConfig(level=logging.INFO, format=log_fmt)
logger = logging.getLogger(__name__)
logger.info("Initialising application with name 'app'")
app = FastAPI()
logger.info("Exposing 'app' in Prometheus")
Instrumentator().instrument(app).expose(app)
logger.info("Loading model")
try:
    model = Receipts_Classification_Model(
        os.environ.get("MODEL_NAME"), os.environ.get("MODEL_STAGE")
    )
except Exception as e:
    logger.fatal("Unexpected error while loading model.")
    logger.exception(e)
    sys.exit(1)


@app.post("/invocations")
async def predict(file: UploadFile):
    """
    A POST endpoint with path '/invocations'.
    Receives a .csv file with targets and returns a .csv file with labels.

    Args:
        file (UploadFile): A .csv file containing targets.

    Returns:
        The .csv file containing predicted labels.

    Raises:
        HTTPException: If the file is not of an expected format.
    """
    logger.info("Received prediction request")
    # Check if file is an image
    ct = file.content_type
    logger.info(ct)
    try:
        csv = file.file
        logger.info("Preparing data for processing...")
        df = prepare_data(csv, "tc", use_targets=False)
        logger.info("Starting processing...")
        result_df = model.predict(df)
        stream = io.StringIO()
        logger.info("Done processing.")
        result_df.to_csv(stream, index=False)
        response = StreamingResponse(iter([stream.getvalue()]), media_type="text/csv")
        response.headers["Content-Disposition"] = "attachment; filename=result.csv"
        return response
    except Exception as e:
        logger.error("Unexpected error while processing the prediction request.")
        logger.exception(e)
        raise HTTPException(
            status_code=500,
            detail="Unexpected error while processing the prediction request.",
        )
