import logging

import mlflow
import pandas as pd
import torch
from dotenv import load_dotenv
from pandarallel import pandarallel
from tqdm.auto import tqdm

from src.data.constants import (
    COLUMN_BRAND,
    COLUMN_GOOD,
    COLUMN_ID,
    COLUMN_TOKENIZED_DESCRIPTION,
    COLUMN_TOKENIZED_TAGS,
    TAG_BRAND_BEGINNING,
    TAG_BRAND_INSIDE,
    TAG_GOOD_BEGINNING,
    TAG_GOOD_INSIDE,
)


class Receipts_Classification_Model:
    def __init__(self, model_name: str, model_stage: str):
        """
        Initializes an instance of the class with a trained model from the mlflow registry.

        Args:
            model_name (str): Model name in mlflow registry.
            model_stage (str): Model stage.
        """
        load_dotenv()
        log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
        logging.basicConfig(level=logging.INFO, format=log_fmt)
        components = mlflow.transformers.load_model(
            model_uri=f"models:/{model_name}/{model_stage}",
            return_type="components",
            device=0,
        )
        self.tokenizer = components["tokenizer"]
        self.model = components["model"]
        # TODO: right now the model is loaded once, when the service starts. We probably want a script
        # that will periodically check if there is a newer version in MLFlow and use it instead.

    def predict(
        self,
        input_df: pd.DataFrame,
        batch_size: int = 100,
        max_rows: int = 0,
        drop_duplicates: bool = True,
    ) -> pd.DataFrame:
        assert isinstance(input_df, pd.DataFrame)
        assert isinstance(batch_size, int)
        assert isinstance(max_rows, int)
        assert isinstance(drop_duplicates, bool)

        pandarallel.initialize(progress_bar=True)

        device = "cuda:0" if torch.cuda.is_available() else "cpu"

        df = input_df

        # limit max_rows for situations where we don't want to work with the entire dataset
        if max_rows != 0:
            df = df.iloc[:max_rows]

        # feed this to tokenizer in batches
        values = df[COLUMN_TOKENIZED_DESCRIPTION].values

        logit_batches_by_model = []
        for model in [self.model]:
            tokenizer = self.tokenizer
            model = model.to(device)

            current_batch = 0
            logit_batches = []
            predicted_classes = []
            word_ids = []
            for current_batch in tqdm(range(0, values.shape[0], batch_size)):
                # convert np.array to list to feed to the tokenizer
                batch = values[current_batch : current_batch + batch_size].tolist()
                # get tensors to run model on
                inputs = tokenizer(
                    batch, return_tensors="pt", padding=True, is_split_into_words=True
                ).to(device)
                # get ids of words to later match tokens with words
                word_ids.extend(
                    [inputs.word_ids(i) for i in range(len(inputs["input_ids"]))]
                )

                with torch.no_grad():
                    logits = model(**inputs).logits

                logit_batches.append(logits)

            logit_batches_by_model.append(logit_batches)

        stacked_batches = [torch.stack(t) for t in zip(*logit_batches_by_model)]
        mean_batches = [batch.mean(dim=0) for batch in stacked_batches]

        for logit_batch in mean_batches:
            predictions = torch.argmax(logit_batch, dim=2).to(device)

            # convert predictions from tensors to readable tags
            predicted_classes.extend(
                [[model.config.id2label[e.item()] for e in t] for t in predictions]
            )
        df[COLUMN_TOKENIZED_TAGS] = predicted_classes
        df["word_ids"] = word_ids

        if COLUMN_GOOD in df.columns:
            df.drop(COLUMN_GOOD, axis=1, inplace=True)
        if COLUMN_BRAND in df.columns:
            df.drop(COLUMN_BRAND, axis=1, inplace=True)

        df = df.parallel_apply(
            self._find_goods_and_brands,
            axis=1,
            drop_duplicates=drop_duplicates,
        )

        return df[[COLUMN_ID, COLUMN_GOOD, COLUMN_BRAND]]

    @staticmethod
    def _make_sentences(
        row, tag_beginning: str, tag_inside: str, drop_duplicates: bool = False
    ) -> str:
        sentences = []
        current_sentence = None
        current_word_id = None
        for label, word_id in zip(row[COLUMN_TOKENIZED_TAGS], row["word_ids"]):
            if word_id is None:
                # can't do anything with labels not assigned to words
                continue
            if label == tag_beginning:
                # words with this tag always start new sentences
                if word_id != current_word_id:
                    # add completed sentence to sentences
                    if current_sentence is not None:
                        sentences.append(current_sentence)
                    current_sentence = row[COLUMN_TOKENIZED_DESCRIPTION][word_id]
                    current_word_id = word_id
            elif label == tag_inside:
                # words with this tag should be preceded by other words in a sentence
                # but sometimes model fails to do so
                if word_id != current_word_id:
                    # add a word to the current sentence
                    if current_sentence is not None:
                        current_sentence = " ".join(
                            [
                                current_sentence,
                                row[COLUMN_TOKENIZED_DESCRIPTION][word_id],
                            ]
                        )
                    else:
                        current_sentence = row[COLUMN_TOKENIZED_DESCRIPTION][word_id]
                    current_word_id = word_id
            else:
                if (current_word_id is not None) and (word_id != current_word_id):
                    # we moved on to the next word and it is not of a wanted tag
                    # add completed sentence to sentences
                    if current_sentence is not None:
                        sentences.append(current_sentence)
                    current_sentence = None
                    current_word_id = None
        if current_sentence is not None:
            sentences.append(current_sentence)

        if drop_duplicates:
            seen = set()
            sentences = [s for s in sentences if not (s in seen or seen.add(s))]

        return ",".join(sentences)

    @staticmethod
    def _find_goods_and_brands(
        row,
        search_tags=True,
        drop_duplicates=False,
    ):
        if search_tags:
            row[COLUMN_GOOD] = Receipts_Classification_Model._make_sentences(
                row,
                tag_beginning=TAG_GOOD_BEGINNING,
                tag_inside=TAG_GOOD_INSIDE,
                drop_duplicates=drop_duplicates,
            )
            row[COLUMN_BRAND] = Receipts_Classification_Model._make_sentences(
                row,
                tag_beginning=TAG_BRAND_BEGINNING,
                tag_inside=TAG_BRAND_INSIDE,
                drop_duplicates=drop_duplicates,
            )
        return row
